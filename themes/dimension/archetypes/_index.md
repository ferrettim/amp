---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
description: "Connecting philosophy and spirituality to the human experience"
background: "/images/bg.jpg"
logo: "eye"
---
